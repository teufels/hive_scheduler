<?php

namespace HIVE\HiveScheduler\Task;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Core\Environment;

/**
 * This file is part of the "hive_scheduler" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 **/

/**
 * Class LogFileHandlingTask.
 */
class LogFileHandlingTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{

    /**
     * @var string $email
     */
    public $email;

    /**
     * @var string $maxLogSize
     */
    public $maxLogSize;

    /**
     * @var string $thresholdWarning
     */
    public $thresholdWarning;

    /**
     * @var string $keepLogFile
     */
    public $keepLogFile;

    /**
     * @var string $logFolder
     */
    public $logFolder;


    public function execute() {
        $success = true;

        $projectPath = Environment::getProjectPath();
        $logDir = $projectPath . $this->logFolder;
        
        $logFilesWarning = array();

        foreach (glob($logDir . "*.log") as $file) {

            //1. get file sizes
            $filesize = filesize($file); // bytes
            $filesize = round($filesize / 1024 / 1024, 1); // megabytes with 1 digit

            //2. delete log files older $keepLogFile AND filesize > $maxLogSize
            $lastModifiedTimestamp = filemtime($file);
            $keepDays = intval($this->keepLogFile);
            if(  ($filesize >= floatval($this->maxLogSize)) && (strtotime($lastModifiedTimestamp) >= (time() - (60*60*24*$keepDays))) ){
                if (!unlink($file)) {
                    $this->logger->warning('[HIVE\\HiveScheduler\\Task\\LogFileHandlingTask]: ' . $file . ' cannot be deleted due to an error');
                }
                else {
                    $success = false;
                    $this->logger->info('[HIVE\\HiveScheduler\\Task\\LogFileHandlingTask]: ' . $file . ' has been deleted ');
                }
            }
            //3. mv files filesize > $maxLogSize
            else if ( $filesize >= floatval($this->maxLogSize) ) {
                //Move file
                $path = dirname($file);
                $name = basename($file);

                if (strpos($name, "_typo3") == false) {
                    $new = $path . "/" . date("Ymd") . "_" . $name;
                } else {
                    $new = $file;
                    $logFilesWarning[] = $new;
                }

                rename($file, $new);
            }
            //4a. send warning if critical file size reached
            else if ( $filesize >= floatval($this->thresholdWarning) ) {
                $logFilesWarning[] = $file;
            }

        }

        //4b. send warning if critical file size reached
        if ( !empty($this->email) && !empty($logFilesWarning) ) {

            // If an email address is defined, send a message to it
            //$this->logger->info('[HIVE\\HiveScheduler\\Task\\LogFileHandlingTask]: email sent to "' . $this->email . '"');

            $site_url = GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
            $server_host = $_SERVER['HTTP_HOST'];

            $mailBody = 'HIVE SCHEDULER TASK: Log-File handling' . LF;
            $mailBody .= 'FROM: ' . $server_host . ' <' . $site_url . '>' . LF;
            $mailBody .= '- - - - - - - - - - - - - - - - - - - - - -' . LF;
            $mailBody .= date("Y-m-d"). ' ' . 'Log-File(s) reached critical filesize:' . LF;

            foreach ($logFilesWarning as $file) {
                $filesize = filesize($file); // bytes
                $filesize = round($filesize / 1024 / 1024, 1); // megabytes with 1 digit
                $mailBody .= "file: " . $file . ' size: ' .$filesize . ' MB' . LF;
            }


            // Prepare mailer and send the mail
            try {
                /** @var \TYPO3\CMS\Core\Mail\MailMessage $mailer */
                $mailer = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
                $mailer->setFrom([$this->email => 'HIVE SCHEDULER TASK']);
                $mailer->setReplyTo([$this->email => 'HIVE SCHEDULER TASK']);
                $mailer->setSubject('HIVE SCHEDULER TASK: Log-File handling FROM ' . $server_host);
                $mailer->setBody()->text($mailBody);
                $mailer->setTo($this->email);
                $mailer->send();
            } catch (\Exception $e) {
                $success = false;
                throw new \TYPO3\CMS\Core\Exception($e->getMessage(), 1476048416);
            }

        } else {
            if ( empty($this->email) ) {
                // No email defined, just log the task
                $this->logger->warning('[HIVE\\HiveScheduler\\Task\\LogFileHandlingTask]: No email address given');
            }

            if ( !empty($logFilesWarning) ) {
                //log Warning if could not be send to mail
                $this->logger->warning('[HIVE\\HiveScheduler\\Task\\LogFileHandlingTask]: Log-File(s) reached critical filesize');
            }

        }

        return $success;
    }


    /**
     * This method returns the fields as additional information
     *
     * @return string Information to display
     */
    public function getAdditionalInformation()
    {
        $text = '';

        $text .= $this->email;
        $text .= ', '.$this->maxLogSize;
        $text .= ', '.$this->thresholdWarning;
        $text .= ', '.$this->keepLogFile;
        $text .= ', '.$this->logFolder;

        return $text;
    }
}