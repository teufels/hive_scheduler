<?php

namespace HIVE\HiveScheduler\Task;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Scheduler\AbstractAdditionalFieldProvider;
use TYPO3\CMS\Scheduler\Controller\SchedulerModuleController;
use TYPO3\CMS\Scheduler\Task\AbstractTask;
use TYPO3\CMS\Scheduler\Task\Enumeration\Action;

/**
 * This file is part of the "hive_scheduler" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 **/

const MAX_LOGFILE_SIZE_MB = 40;
const THRESHOLD_WARNING_MB = 30;
const KEEP_LOGFILE_DAYS = 3;
const LOG_FOLDER = '/var/log/';

/**
 * Class LogFileHandlingAdditionalFieldProvider.
 */
class LogFileHandlingAdditionalFieldProvider extends AbstractAdditionalFieldProvider
{

    /**
     * @var LanguageService
     */
    protected $languageService;

    /**
     * Construct class.
     */
    public function __construct()
    {
        $this->languageService = $GLOBALS['LANG'];
    }

    /**
     * This method is used to define new fields for adding or editing a task
     * In this case, it adds an email field
     *
     * @param array $taskInfo Reference to the array containing the info used in the add/edit form
     * @param AbstractTask|null $task When editing, reference to the current task. NULL when adding.
     * @param SchedulerModuleController $schedulerModule Reference to the calling object (Scheduler's BE module)
     * @return array Array containing all the information pertaining to the additional fields
     */
    public function getAdditionalFields(array &$taskInfo, $task, SchedulerModuleController $schedulerModule)
    {
        $currentSchedulerModuleAction = $schedulerModule->getCurrentAction();

        // Initialize extra field value: email
        if (empty($taskInfo['email'])) {
            if ($currentSchedulerModuleAction->equals(Action::ADD)) {
                // In case of new task and if field is empty, set default email address
                $taskInfo['email'] = $GLOBALS['BE_USER']->user['email'];
            } elseif ($currentSchedulerModuleAction->equals(Action::EDIT)) {
                // In case of edit, and editing a task, set to internal value if not data was submitted already
                $taskInfo['email'] = $task->email;
            } else {
                // Otherwise set an empty value, as it will not be used anyway
                $taskInfo['email'] = '';
            }
        }

        // Initialize extra field value: maxLogSize
        if (empty($taskInfo['maxLogSize'])) {
            if ($currentSchedulerModuleAction->equals(Action::ADD)) {
                // In case of new task and if field is empty, set default maxFileSize
                $taskInfo['maxLogSize'] = MAX_LOGFILE_SIZE_MB;
            } elseif ($currentSchedulerModuleAction->equals(Action::EDIT)) {
                // In case of edit, and editing a task, set to internal value if not data was submitted already
                $taskInfo['maxLogSize'] = $task->maxLogSize;
            } else {
                // Otherwise set an default value
                $taskInfo['maxLogSize'] = MAX_LOGFILE_SIZE_MB;
            }
        }

        // Initialize extra field value: thresholdWarning
        if (empty($taskInfo['thresholdWarning'])) {
            if ($currentSchedulerModuleAction->equals(Action::ADD)) {
                // In case of new task and if field is empty, set default maxFileSize
                $taskInfo['thresholdWarning'] = THRESHOLD_WARNING_MB;
            } elseif ($currentSchedulerModuleAction->equals(Action::EDIT)) {
                // In case of edit, and editing a task, set to internal value if not data was submitted already
                $taskInfo['thresholdWarning'] = $task->thresholdWarning;
            } else {
                // Otherwise set an default value
                $taskInfo['thresholdWarning'] = THRESHOLD_WARNING_MB;
            }
        }

        // Initialize extra field value: keepLogFile
        if (empty($taskInfo['keepLogFile'])) {
            if ($currentSchedulerModuleAction->equals(Action::ADD)) {
                // In case of new task and if field is empty, set default maxFileSize
                $taskInfo['keepLogFile'] = KEEP_LOGFILE_DAYS;
            } elseif ($currentSchedulerModuleAction->equals(Action::EDIT)) {
                // In case of edit, and editing a task, set to internal value if not data was submitted already
                $taskInfo['keepLogFile'] = $task->keepLogFile;
            } else {
                // Otherwise set an default value
                $taskInfo['keepLogFile'] = KEEP_LOGFILE_DAYS;
            }
        }

        // Initialize extra field value: logFolder
        if (empty($taskInfo['logFolder'])) {
            if ($currentSchedulerModuleAction->equals(Action::ADD)) {
                // In case of new task and if field is empty, set default log folder
                $taskInfo['logFolder'] = LOG_FOLDER;
            } elseif ($currentSchedulerModuleAction->equals(Action::EDIT)) {
                // In case of edit, and editing a task, set to internal value if not data was submitted already
                $taskInfo['logFolder'] = $task->logFolder;
            } else {
                // Otherwise set an default value
                $taskInfo['logFolder'] = LOG_FOLDER;
            }
        }


        $additionalFields = [];

        // Render field: email
        $fieldID = 'task_logfilehandling_email';
        $fieldCode = '<input type="text" class="form-control" name="tx_scheduler[email]" id="' . $fieldID . '" value="' . htmlspecialchars($taskInfo['email']) . '" size="30">';
        $additionalFields[$fieldID] = [
            'code' => $fieldCode,
            'label' => BackendUtility::wrapInHelp('hive_scheduler', $fieldID, $this->translate('addfields_label_email')),
            'cshKey' => '_MOD_system_txschedulerM1',
            'cshLabel' => $fieldID
        ];

        // Render field: maxLogSize
        $fieldID = 'task_logfilehandling_maxLogSize';
        $fieldCode = '<input type="text" class="form-control" name="tx_scheduler[maxLogSize]" id="' . $fieldID . '" value="' . htmlspecialchars($taskInfo['maxLogSize']) . '" size="30">';
        $additionalFields[$fieldID] = [
            'code' => $fieldCode,
            'label' => BackendUtility::wrapInHelp('hive_scheduler', $fieldID, $this->translate('addfields_label_maxLogSize')),
            'cshKey' => '_MOD_system_txschedulerM1',
            'cshLabel' => $fieldID
        ];

        // Render field: thresholdWarning
        $fieldID = 'task_logfilehandling_thresholdWarning';
        $fieldCode = '<input type="text" class="form-control" name="tx_scheduler[thresholdWarning]" id="' . $fieldID . '" value="' . htmlspecialchars($taskInfo['thresholdWarning']) . '" size="30">';
        $additionalFields[$fieldID] = [
            'code' => $fieldCode,
            'label' => BackendUtility::wrapInHelp('hive_scheduler', $fieldID, $this->translate('addfields_label_thresholdWarning')),
            'cshKey' => '_MOD_system_txschedulerM1',
            'cshLabel' => $fieldID
        ];

        // Render field: keepLogFile
        $fieldID = 'task_logfilehandling_keepLogFile';
        $fieldCode = '<input type="text" class="form-control" name="tx_scheduler[keepLogFile]" id="' . $fieldID . '" value="' . htmlspecialchars($taskInfo['keepLogFile']) . '" size="30">';
        $additionalFields[$fieldID] = [
            'code' => $fieldCode,
            'label' => BackendUtility::wrapInHelp('hive_scheduler', $fieldID, $this->translate('addfields_label_keepLogFile')),
            'cshKey' => '_MOD_system_txschedulerM1',
            'cshLabel' => $fieldID
        ];

        // Render field: logFolder
        $fieldID = 'task_logfilehandling_logFolder';
        $fieldCode = '<input type="text" class="form-control" name="tx_scheduler[logFolder]" id="' . $fieldID . '" value="' . htmlspecialchars($taskInfo['logFolder']) . '" size="30">';
        $additionalFields[$fieldID] = [
            'code' => $fieldCode,
            'label' => BackendUtility::wrapInHelp('hive_scheduler', $fieldID, $this->translate('addfields_label_logFolder')),
            'cshKey' => '_MOD_system_txschedulerM1',
            'cshLabel' => $fieldID
        ];

        return $additionalFields;
    }

    /**
     * This method checks any additional data that is relevant to the specific task
     * If the task class is not relevant, the method is expected to return TRUE
     *
     * @param array	 $submittedData Reference to the array containing the data submitted by the user
     * @param SchedulerModuleController $schedulerModule Reference to the calling object (Scheduler's BE module)
     * @return bool TRUE if validation was ok (or selected class is not relevant), FALSE otherwise
     */
    public function validateAdditionalFields(array &$submittedData, SchedulerModuleController $schedulerModule)
    {

        //email
        $submittedData['email'] = trim($submittedData['email']);
        if (empty($submittedData['email'])) {
            $this->addMessage(
                $this->translate('addfields_validate_email'),
                FlashMessage::ERROR
            );
            $result = false;
        } else {
            $result = true;
        }

        //for other fields default values are existing so not mandatory

        return $result;
    }

    /**
     * This method is used to save any additional input into the current task object
     * if the task class matches
     *
     * @param array $submittedData Array containing the data submitted by the user
     * @param \TYPO3\CMS\Scheduler\Task\AbstractTask $task Reference to the current task object
     */
    public function saveAdditionalFields(array $submittedData, AbstractTask $task)
    {
        $task->email = $submittedData['email'];
        $task->maxLogSize = $submittedData['maxLogSize'];
        $task->thresholdWarning = $submittedData['thresholdWarning'];
        $task->keepLogFile = $submittedData['keepLogFile'];
        $task->logFolder = $submittedData['logFolder'];
    }

    /**
     * Translate by key.
     *
     * @param string $key
     * @param string $prefix
     *
     * @return string
     */
    protected function translate($key, $prefix = 'LLL:EXT:hive_scheduler/Resources/Private/Language/locallang.xml:')
    {
        return $this->languageService->sL($prefix.$key);
    }
}