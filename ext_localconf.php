<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['HIVE\\HiveScheduler\\Task\\LogFileHandlingTask'] = [
    'extension' => 'hive_scheduler',
    'title' => 'LLL:EXT:hive_scheduler/Resources/Private/Language/locallang.xml:localconf_logFileHandling_title',
    'description' => 'LLL:EXT:hive_scheduler/Resources/Private/Language/locallang.xml:localconf_logFileHandling_description',
    'additionalFields' => 'HIVE\\HiveScheduler\\Task\\LogFileHandlingAdditionalFieldProvider',
];
