<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "hive_scheduler"
 *
 * Auto generated by Extension Builder 2020-03-13
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'HIVE>Scheduler',
    'description' => 'provide scheduler tasks',
    'category' => 'plugin',
    'author' => 'Albert Giss, Alexander Neher, Andreas Hafner, Bastian Holzem, Hendrik Krueger, Marcel Weber, Timo Bittner, Zoe Kern',
    'author_email' => 'a.giss@teufels.com, a.neher@teufels.com, a.hafner@teufels.com, b.holzem@teufels.com, h.krueger@teufels.com, m.weber@teudels.com, t.bittner@teufels.com, z.kern@teufels.com',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '11.1.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.9.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
